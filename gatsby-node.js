const path = require("path");
exports.createPages = async ({ graphql, actions }) => {
  const { data } = await graphql(`
    query ProjectPage {
      allMarkdownRemark {
        nodes {
          frontmatter {
            company
          }
        }
      }
    }
  `);

  data.allMarkdownRemark.nodes.forEach((node) => {
    actions.createPage({
      path: "/projects/" + node.frontmatter.company,
      component: path.resolve("./src/templates/project-details.js"),
      contenxt: { company: node.frontmatter.company },
    });
  });
};
