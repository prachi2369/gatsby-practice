GatsBy
--------



Content Mesh &  GraphQL
-------------
Gatsby brings all data sources together into a single graphql layer is call content mesh

Page Query & useStaticQuery
---------------
useStaticQuery -
we can you only once on each component
use inside component code structure

page query-
write after code structure complete/outside of code structure.


Source Plugins
---------------


Markdown & Transformer Plugins
------------------------------

Sorting / Ordering Queries
------------------------

Multiple Queries
----------------

Optimized Image
------------

Featured Image
-------------

Template Components
-------------------

Generating Pages
-----------------

Query variable
---------------