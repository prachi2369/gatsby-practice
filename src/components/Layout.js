import Navbar from "./Navbar";
import React from "react";
import "../styles/global.css";
export default function Layout({ children }) {
  return (
    <div className="layout">
      <Navbar />
      <div className="content">{children}</div>
      <footer>
        <p>Footer - Copyright 2024 Prachi Sanghavi</p>
      </footer>
    </div>
  );
}
