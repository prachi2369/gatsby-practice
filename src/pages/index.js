import * as React from "react";
import Layout from "../components/Layout";
import * as styles from "../styles/home.module.css";
import { Link, graphql } from "gatsby";
import Img from "gatsby-image";
export default function Home({ data }) {
  const { title, description } = data.site.siteMetadata;
  return (
    <Layout>
      <section className={styles.header}>
        <div>
          <h2>Design!!!!</h2>
          <h3>Develop & design</h3>
          <p>Mernstack developer</p>
          <Link className={styles.btn} to="/projects">
            My ProtFolio Projects
          </Link>
        </div>
        {/* <img src="/img1.jpg" alt="banner img" style={{ maxWidth: "100%" }} /> */}
        <Img fluid={data.file.childImageSharp.fluid} />
        <p>
          {title} - {description}
        </p>
      </section>
    </Layout>
  );
}

export const query = graphql`
  query SiteInfo {
    site {
      siteMetadata {
        copyright
        description
        title
      }
    }
    file(relativePath: { eq: "img1.jpg" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;
